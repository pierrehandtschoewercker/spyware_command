#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>

int main(void)
{
	char buffer[2048] = { 0 };
	struct sockaddr_in sin = { 0 };
    	int                bytes = 0;

	int sock = socket(AF_INET, SOCK_STREAM, 0);

	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = inet_addr("127.0.0.1");
	sin.sin_port = htons(8788);
	memset((void*)sin.sin_zero, 0, 8);

	printf("connect = %d\n", connect(sock, (struct sockaddr*) & sin, sizeof(sin)));

	while(1)
	{
		memset(buffer, 0, 2048);
		bytes = recv(sock, buffer, 2048, 0);
		printf("|%d|%s|\n", bytes, buffer);
		if(bytes == 0)
			break;
	}
}
