<?php
	# N'est plus utilisé actuellement car ce n'est plus nécessaire
	echo($_SERVER['SERVER_NAME'].":");
	echo($_SERVER['SERVER_PORT']);

	if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "" || $_SERVER['HTTPS'] == "off")
	{
		$redirect = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		header("HTTP/1.1 301 Moved Permanently");
		header("Location: $redirect");
		exit;
	}