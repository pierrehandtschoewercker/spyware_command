<?php

$file = fopen('./requetes.txt','a+') or die("can't open file");
$date = date("Y-m-d H:i:s");

if (isset($_GET)&&!empty($_GET))
{
    fwrite($file,"[".$date."]\n [GET] : ".print_r($_GET,true)."\n");
}

if(isset($_POST) && !empty($_POST))
{
    fwrite($file,"[".$date."][POST] : [\n".print_r($_POST, true)."\n");
}

if(isset($_FILES) && !empty($_FILES))
{
    fwrite($file,"[".$date."][FILES] : [\n".print_r($_FILES, true)."\n");
}


