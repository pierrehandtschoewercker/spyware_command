<?php
require __DIR__ . '/vendor/autoload.php';

use Ratchet\Server\IoServer;
use MyApp\Traduction;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

$server = new HttpServer(
    new WsServer(
        new Traduction()
    )
);

$loop = \React\EventLoop\Factory::create();

$secure_websockets = new \React\Socket\Server('0.0.0.0:9003', $loop);
$secure_websockets = new \React\Socket\SecureServer($secure_websockets, $loop, [
    'local_cert' => '/etc/ssl/certs/ssl-cert-snakeoil.pem',
    'local_pk' => '/etc/ssl/private/ssl-cert-snakeoil.key',
    'verify_peer' => false,
    'allow_self_signed' => true
]);


$secure_websockets_server = new IoServer($server, $secure_websockets, $loop);
$secure_websockets_server->run();