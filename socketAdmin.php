<?php
error_reporting(E_ALL);
set_time_limit(0);
ob_implicit_flush();
$address = '127.0.0.1';
$port = 9000;

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
	echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}

$msgsock = socket_connect($sock, $address, $port);
//$stdin = fopen("php://stdin",'r');
socket_write($sock, "/screenshot\n", 2048);
socket_write($sock, "/keylogger\n", 2048);

socket_close($sock);