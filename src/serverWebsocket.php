<?php
namespace MyApp;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

error_reporting(E_ALL);
set_time_limit(0);
ob_implicit_flush();

class Traduction implements MessageComponentInterface
{

    protected $admins;

    public function __construct()
    {
        $this->admins = new \SplObjectStorage;

        $this->address = '127.0.0.1';
        $this->port = 9000;

        if (($this->sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        }

        $this->msgsock = socket_connect($this->sock, $this->address, $this->port);
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->admins->attach($conn);
        print "Nouvelle Connexion {$conn->resourceId}" . PHP_EOL;
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        print "Message {$msg} de {$from->resourceId}" . PHP_EOL;
        socket_write($this->sock, $msg, 2048);
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->admins->detach($conn);
        print "Déconnexion de {$conn->resourceId}" . PHP_EOL;
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        print "Il y a eu une erreur {$conn->resourceId}  erreur: {$e->getMessage()}";
        $conn->close();
    }
}
