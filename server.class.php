<?php

use Amp\Socket\Socket;
use function Amp\asyncCall;

class Server
{
    private $uri = "tcp://0.0.0.0:9001";
    private $adminuri = "tcp://127.0.0.1:9000";

    // We use a property to store a map of $clientAddr => $client
    private $clients = [];
    private $admins = [];

    // Store a $clientAddr => $username map
    //private $usernames = [];

    public function listenClient()
    {
        asyncCall(function () {
            $server = Amp\Socket\Server::listen($this->uri);

            print "Listening on " . $server->getAddress() . " ..." . PHP_EOL;

            while ($socket = yield $server->accept()) {
                $this->handleClient($socket);
            }
        });
    }

    public function listenAdmin()
    {
        asyncCall(function () {
            $server = Amp\Socket\Server::listen($this->adminuri);

            print "Listening on " . $server->getAddress() . " ..." . PHP_EOL;

            while ($socket = yield $server->accept()) {
                $this->handleAdmin($socket);
            }
        });
    }

    private function handleClient(Socket $socket)
    {
        asyncCall(function () use ($socket) {
            $remoteAddr = $socket->getRemoteAddress();

            // We print a message on the server and send a message to each client
            print "Accepted new client: {$remoteAddr}" . PHP_EOL;

            // We only insert the client afterwards, so it doesn't get its own join message
            $this->clients[(string) $remoteAddr] = $socket;

            $buffer = "";

            while (null !== $chunk = yield $socket->read()) {
                $buffer .= $chunk;

                while (($pos = strpos($buffer, "\n")) !== false) {
                    //$this->handleMessage($socket, substr($buffer, 0, $pos));
                    print "Client {$remoteAddr} says : " . $buffer;
                    $buffer = substr($buffer, $pos + 1);
                }
            }

            // We remove the client again once it disconnected.
            // It's important, otherwise we'll leak memory.
            // We also have to unset our new usernames.
            unset($this->clients[(string) $remoteAddr]);

            // Inform other clients that that client disconnected and also print it in the server.
            print "Client disconnected: {$remoteAddr}" . PHP_EOL;
            //$this->broadcast(($this->usernames[(string) $remoteAddr] ?? $remoteAddr) . " left the chat." . PHP_EOL);
        });
    }

    private function handleAdmin(Socket $socket)
    {
        asyncCall(function () use ($socket) {
            $remoteAddr = $socket->getRemoteAddress();

            // We print a message on the server and send a message to each client
            print "Accepted new admin: {$remoteAddr}" . PHP_EOL;
            //$this->broadcast($remoteAddr . " joined the chat." . PHP_EOL);

            // We only insert the client afterwards, so it doesn't get its own join message
            $this->admins[(string) $remoteAddr] = $socket;
            //print_r($this->admins);

            $buffer = "";

            while (null !== $chunk = yield $socket->read()) {
                $buffer .= $chunk;
                //print "Admin says {$buffer}" . PHP_EOL;

                while (($pos = strpos($buffer, "\n")) !== false) {
                    //print $pos . PHP_EOL;
                    //print "Admin says " . substr($buffer, 0, $pos) . PHP_EOL;

                    $this->handleMessage($socket,  substr($buffer, 0, $pos));
                    $buffer = substr($buffer, $pos + 1);
                }
            }

            // We remove the client again once it disconnected.
            // It's important, otherwise we'll leak memory.
            // We also have to unset our new usernames.
            unset($this->admins[(string) $remoteAddr], $this->usernames[(string) $remoteAddr]);

            // Inform other clients that that client disconnected and also print it in the server.
            print "Admin disconnected: {$remoteAddr}" . PHP_EOL;
            //$this->broadcast(($this->usernames[(string) $remoteAddr] ?? $remoteAddr) . " left the chat." . PHP_EOL);
        });
    }

    private function handleMessage(Socket $socket, string $message, $isBroadCast = false)
    {
        if ($message === "") {
            // ignore all empty messages
            return;
        }

        print $message . PHP_EOL;

        $split = explode("/", $message);
        print_r($split);

        if (isset($this->clients[$split[0]]) && count($split) >= 2) {
            $this->sendToClient($this->clients[$split[0]], $split[count($split) - 1]);
        } else {
            $this->broadcast($split[count($split) - 1]);
        }


        return;


        /*$remoteAddr = $socket->getRemoteAddress();
            $user = $this->usernames[(string) $remoteAddr] ?? $remoteAddr;
            $this->broadcast($user . " says: " . $message . PHP_EOL);*/
    }

    private function sendToClient($socket, $message)
    {
        $socket->write($message);
    }

    private function broadcast(string $message)
    {
        foreach ($this->clients as $client) {
            // We don't yield the promise returned from $client->write() here as we don't care about
            // other clients disconnecting and thus the write failing.
            //print_r($client);
            $client->write($message);
        }
    }
}
