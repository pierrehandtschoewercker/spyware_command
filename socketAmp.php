<?php

require __DIR__ . "/vendor/autoload.php";
require __DIR__ . "/server.class.php";

// Non-blocking server implementation based on amphp/socket keeping track of connections.

use Amp\Loop;
use Amp\Socket\Socket;
use function Amp\asyncCall;

Loop::run(function () {
    $server = new Server();
    $server->listenClient();
    $server->listenAdmin();
});
